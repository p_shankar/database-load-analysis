file = 'poolD' ;
fir = importdata(strcat(file,'/1.dat'));
sec = importdata(strcat(file,'/2.dat'));
th_a = importdata(strcat(file,'/3a.dat'));
th_b = importdata(strcat(file,'/3b.dat'));
th_c = importdata(strcat(file,'/3c.dat'));

figure
plot(fir, 'c-o');
hold on
plot(sec, 'b-o');
hold on
plot(th_a, 'g-o');
hold on
plot(th_b, 'r-o');
hold on
plot(th_c, 'm-o');
set(gca,'XTickLabel',{'10:1','10:2','10:3','10:4','10:5','10:6','10:7', '10:8', '10:9', '10:10'});
legend('Load classification','Load mixing','Ladies first-FIFO', 'Ladies first-smallest first', 'Ladies first-waited first');
ylabel('Worst case performance');
xlabel('DG capacity:FC capacity');
hold off