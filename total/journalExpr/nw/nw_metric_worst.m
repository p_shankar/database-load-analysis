
x = [2.26085, 1.52151, 1.78667, 2.64286, 8.21834;
     2.26229, 1.52151, 1.78667, 2.64286, 43.3353;
     2.24958, 1.7541,  1.78667, 3.39394, 8.21834;
     2.26085, 2.5,     1.78667, 3.56061, 8.21834;];

 figure
bar(x, 'grouped');
set(gca,'XTickLabel',{'no error','sigma = 0.5','sigma = 1','sigma = 1.5'})
legend('HPC2N', 'LANL', 'PIK', 'SDSC-BLUE', 'SDSC-DS');
xlabel('error rate');
ylabel('worst performance');


