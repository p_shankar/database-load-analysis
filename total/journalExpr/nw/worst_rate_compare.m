
x = [2.26085, 1.52151, 1.78667,   2.64286, 8.21834;
     2.62766, 5.75,    3.82353,   8.47619, 35.3254;
     18.8,    50.1735, 20.5973,   51.194,  2.76938;
     78.4359, 183.636, 122.253,   117,     14.2058;];

 figure
bar(x, 'grouped');
set(gca,'XTickLabel',{'no error','sigma = 0.5','sigma = 1','sigma = 1.5'})
legend('HPC2N', 'LANL',  'PIK', 'SDSC-BLUE', 'SDSC-DS');
xlabel('error rate');
ylabel('worst performance');


