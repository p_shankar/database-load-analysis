
x = [2.26085, 1.52151, 2.21453,   4.76,    2.64286;
     3.64701, 5.91667, 6.71267,   2.5,     9.85957;
     9.28571, 38.6145, 30.5167,   7.5,     31.4545;
     65,      739.25,  264.75,    3.96232, 125.35;];

 figure
bar(x, 'grouped');
set(gca,'XTickLabel',{'no error','sigma = 0.5','sigma = 1','sigma = 1.5'})
legend('HPC2N', 'LANL', 'LANL-O2K', 'PIK', 'SDSC-BLUE');
xlabel('error rate');
ylabel('worst performance');


