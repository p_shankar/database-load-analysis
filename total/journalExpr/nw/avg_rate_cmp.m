
x = [1.02156, 1.00653, 1.00482, 2.16439;
     1.12505, 1.12686, 1.11989, 6.30743;
     1.86617, 1.69567, 1.58104, 1.63228;
     3.07523, 3.09567, 3.04581, 4.15538;];

 figure
bar(x, 'grouped');
set(gca,'XTickLabel',{'no error','sigma = 0.5','sigma = 1','sigma = 1.5'})
legend('HPC2N', 'LANL', 'PIK', 'SDSC-BLUE', 'SDSC-DS');
xlabel('error rate');
ylabel('worst performance');


