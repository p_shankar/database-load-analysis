
x = [23219, 98636,  40031,  65076;
     23629, 101250, 40424,  68422;
     23515, 105290, 40641,  77280;
     23873, 109489, 41101,  83984;];

 figure
bar(x, 'grouped');
set(gca,'XTickLabel',{'no error','sigma = 0.5','sigma = 1','sigma = 1.5'})
legend('HPC2N', 'LANL', 'LANL-O2K', 'SDSC-BLUE');
xlabel('error rate');
ylabel('DG change rate');


