#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <string.h>
#include <queue>
#include <set>
#include <map>
#define total_time 5000000
#define power_pair pair<int, double>
#define power second 
#define total 742965 
#define interval 600 
#define min_rate  0.6
#define max_rate 1.25
#define p_idle 31.
#define p_peak 93.
#define delay 2 
#define Maintain 1
#define Up 2
#define Down 3
using namespace std;

const double eps = 1e-5;

struct info{
	int start, end;
	double power;
	int latency;
	info(){start = 999999;}
	info(int s, int e, double p){
		start = s; end = e; power = p;	
	}
	bool operator < (const info &a)const{
		if (start == a.start) return end < a.end;	
		else return start < a.start;
	}
}data[total + 5];
double slice_tot;
double avg_t;
int all;
multiset<power_pair> retire;
vector<info> slice_job;
multiset<double> peak_s;
int sum;
double turnaround;
double tt;
double p;

double executeShort(double cur, vector<info>& slice, int now, double limit){
	double tot = 0;
	while (!slice.empty() && limit - cur > 0){
		double mm = (now - slice[0].start + slice[0].end - slice[0].start) / (slice[0].end - slice[0].start);
		int label = 0;
		for (int i = 1; i < slice.size(); i ++){
			if ((now - slice[i].start+ slice[i].end - slice[i].start) / (slice[i].end - slice[i].start) > mm){
				mm = (now - slice[i].start + slice[i].end - slice[i].start) / (slice[i].end - slice[i].start);
				label = i;
			}
		} 

		info head = slice[label];
		if (cur + tot + head.power > limit) break;
		retire.insert(make_pair(now - head.start + head.end, head.power));	
		double latency =  double(now - head.start + head.end - head.start) / double(head.end - head.start) ;
		avg_t += latency; all ++;

		if  (latency > turnaround){
			turnaround = latency;
			tt = head.end - head.start;
			p = head.power;
		}
		tot += head.power;
		slice_tot -= head.power;
		for (int i = label; i < slice.size(); i ++) slice[i] = slice[i + 1];
		slice.pop_back();
	}
	return tot;
}

double executeNow(double cur, vector<info>& slice, int now, double limit){
	double tot = 0;
	while (!slice.empty() && limit - cur > 0){
		double mm = 0; 
		int label = -1;
		for (int i = 0; i < slice.size(); i ++){
			if ((slice[i].end - slice[i].start < 600.) && (now - slice[i].start+ slice[i].end - slice[i].start) / (slice[i].end - slice[i].start) > mm){
				mm = (now - slice[i].start + slice[i].end - slice[i].start) / (slice[i].end - slice[i].start);
				label = i;
			}
		} 

		if (label == -1) break;
		info head = slice[label];
		if (cur + tot + head.power > limit) break;
		retire.insert(make_pair(now - head.start + head.end, head.power));	
		double latency =  double(now - head.start + head.end - head.start) / double(head.end - head.start) ;
		avg_t += latency; all ++;

		if  (latency > turnaround){
			turnaround = latency;
			tt = head.end - head.start;
			p = now - head.start; 
		}
		tot += head.power;
		slice_tot -= head.power;
		for (int i = label; i < slice.size(); i ++) slice[i] = slice[i + 1];
		slice.pop_back();
	}
	return tot;
}

int main(){
	sum = 0;
	double Capacity, Total;
	scanf("%lf", &Total);
	for (int  i = 0; i < total; i ++){
		if (scanf("%d %d %lf", &data[i].start, &data[i].end, &data[i].power) == -1) {sum = i; break;}
	}

	sort(data, data + sum);
	for (int z = 1; z <= 10; z ++){
		double cur = 0;
		int head = 0;
			retire.clear();
				double Mod_rate = Capacity * 0.1 / 60. ; 
		int Ind = Maintain; 
		double budget = 0.;
		turnaround = 1.;
		double peak = 0.;
	double FC_budget = Total - Capacity;
		slice_job.clear();
		avg_t = 0.;all = 0;
		double p_waste = 0., p_total = 0.;

		for (int i = 0; i <= total_time; i ++){
			if (Ind == Up 
					&& budget + Mod_rate < Capacity) budget += Mod_rate;
			if (Ind == Down && budget - Mod_rate > eps) budget -= Mod_rate;
			for (; head < sum && data[head].start == i; head ++){
						slice_job.push_back(data[head]);
				slice_tot += data[head].power;
			}
			cur += executeShort(cur, slice_job, i, budget);		
			if (slice_tot > eps) Ind = Up;
				else if (budget - cur - Mod_rate > eps) Ind = Down; 
					else Ind = Maintain;
			cur += executeNow(cur, slice_job, i, budget + FC_budget);		
			p_total += budget;
			p_waste += (cur < budget) ? budget - cur : 0;

			while (!retire.empty() && (*retire.begin()).first== i + 1){
				cur -= (*retire.begin()).power;
				retire.erase(retire.begin());	
			}	

		} 
		cout<<p_waste/ p_total<<endl;

	}
}

