#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <string.h>
#include <queue>
#include <set>
#include <map>
#define total_time 5000000
#define power_pair pair<int, double>
#define power second 
#define total 742965 
#define interval 600 
#define p_idle 31.
#define p_peak 93.
#define delay 2 
#define Maintain 1
#define Up 2
#define Down 3
using namespace std;

const double eps = 1e-5;

struct info{
	int start, end;
	double power;
	info(){start = 999999;}
	info(int s, int e, double p){
		start = s; end = e; power = p;	
	}
	bool operator < (const info &a)const{
		if (start == a.start) return end < a.end;	
		else return start < a.start;
	}
}data[total + 5];
double slice_tot;
double avg_waiting;
int wtot;
map<int, int> h;
multiset<power_pair> retire;
multiset<power_pair> retire_pieces;
queue<info> slice_job;
queue<info> pieces;
multiset<double> peak_s;
int sum;
double turnaround;

double executeShort(double cur, queue<info>& slice, int now, double limit){
	double tot = 0;
	while (!slice.empty() && limit - cur > 0){
		int label = 0;
		info head = slice.front();
		if (cur + tot + head.power > limit) break;
		retire.insert(make_pair(now - head.start + head.end, head.power));	
		double latency =  double(now - head.start + head.end - head.start) / double(head.end - head.start) ;
		avg_waiting += latency; wtot ++;

		if  (latency > turnaround){
			turnaround = latency;
		}
		tot += head.power;
		slice_tot -= head.power;
		slice.pop();
	}
	return tot;
}

double executePieces(double cur, queue<info>& pieces, int now, double budget){
	double tot = 0;
	while (!pieces.empty() && budget > cur){
		info head = pieces.front();
		if (cur + tot + head.power > budget) break;
		retire_pieces.insert(make_pair(now - head.start + head.end, head.power));	
		double latency =  double(now - head.start + head.end - head.start) / double(head.end - head.start) ;
		avg_waiting += latency; wtot ++;
		if  (latency > turnaround){
			turnaround = latency;
		}
		tot += head.power;
		pieces.pop();

	}
	return tot;
}


int main(){
	h.clear();
	int submit_time, wait_time, run_time;
	double cpu_time;
	int a, b, c, d;
	sum = 0;
	double Capacity;
	scanf("%lf", &Capacity);
	for (int  i = 0; i < total; i ++){
		if (scanf("%d %d %lf", &data[i].start, &data[i].end, &data[i].power) == -1) {sum = i; break;}
	}
	sort(data, data + sum);
	double cur = 0;
	int head = 0;
	int t = 0;
	double max_pow = 0;
	double actual = 0;
	double p_total = 0, p_waste = 0;
	int slice = 60 * 10;
	int nxt_slice = 0;
	double cur_slice = 0;
	int max_slice = 0;
	double cpu;
	retire.clear();
	retire_pieces.clear();
	double wait_sum = 0;
	double last_est = 0;
	double limit;
	double max_surge = 0;
	double pre = 1.;
	double cur_p = 0.;
	double extra = 0;
	avg_waiting = 0;
	wtot = 0;
	int start = 2700000, end = start + 7 * 24 * 60 * 60;	
	double p_extra = 0;

	for (int z = 1; z <= 10; z ++){
		double cur = 0;
		int head = 0;
		int t = 0;
		double max_pow = 0;
		double actual = 0;
		double p_total = 0, p_waste = 0;
		int slice = 60 * 10;
		int nxt_slice = 0;
		double cur_slice = 0;
		int max_slice = 0;
		double cpu;
		retire.clear();
		retire_pieces.clear();
		double wait_sum = 0;
		double last_est = 0;
		double limit;
		double max_surge = 0;
		double pre = 1.;
		double cur_p = 0.;
		double extra = 0;
		avg_waiting = 0;
		wtot = 0;
		int start = 2700000, end = start + 7 * 24 * 60 * 60;	
		double p_extra = 0;
		while (!slice_job.empty()) slice_job.pop();
		while (!pieces.empty()) pieces.pop();

		double Mod_rate = Capacity * 0.1 / 60. ; 
		int Ind = Maintain; 
		double budget = 0.;
		turnaround = 1.;
		double peak = 0.;
		double FC_peak = 0;
		double FC_budget = Capacity / 10. * double(z); 


		for (int i = 0; i <= total_time; i ++){
			if (Ind == Up 
					&& budget + Mod_rate < Capacity) budget += Mod_rate;
			if (Ind == Down && budget - Mod_rate > eps) budget -= Mod_rate;
			for (; head < sum && data[head].start == i; head ++){
				if (data[head].end - data[head].start < interval){
					pieces.push(data[head]);
				}
				else{
					slice_job.push(data[head]);
					slice_tot += data[head].power;
				}
			}
			cur += executeShort(cur, slice_job, i, budget);		
			extra += executePieces(extra, pieces, i, FC_budget);
			double real = cur + extra;	
			p_total += budget;
			peak = max(peak, budget);
			FC_peak = max(FC_peak, extra);
			p_waste += (budget > real) ? budget - real : 0;
			if (slice_tot > eps) Ind = Up;
			else if (budget - cur - Mod_rate > eps) Ind = Down; 
			else Ind = Maintain;

			p_extra += (real > budget) ? real - budget : 0; 
			while (!retire.empty() && (*retire.begin()).first== i + 1){
				cur -= (*retire.begin()).power;
				retire.erase(retire.begin());	
			}	
			while (!retire_pieces.empty() && (*retire_pieces.begin()).first== i + 1){
				extra -= (*retire_pieces.begin()).power;
				retire_pieces.erase(retire_pieces.begin());	
			}	
		} 
		cout<<avg_waiting/double(wtot)<<endl;
	}
}

