#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <string.h>
#include <queue>
#include <set>
#define total 742965 
#define total_time 5000000 
#define power_pair pair<int, double>
#define end first  
#define power second 
using namespace std;

const double eps = 1e-5;

struct info{
	int start, end;
	double power;
	info(){start = 999999;}
	info(int s, int e, double p){
		start = s; end = e; power = p;	
	}
	bool operator < (const info &a)const{
		if (start == a.start) return end < a.end;	
		else return start < a.start;
	}
}data[total];
multiset<power_pair> s;

int main(){
	int submit_time, wait_time, run_time;
	double cpu_time;
	int a, b, c, d;
	double p_peak = 186. / 2., p_idle = 62. /2.;
	double Capacity;
	scanf("%lf", &Capacity);
	int sum = 0;
	int m;
	for (int  i = 0; i < total; i ++){
		if (scanf("%d %d %lf", &data[i].start, &data[i].end, &data[i].power) == -1) {sum = i; break;}
	}

	sort(data, data + sum);
	s.clear();
	double cur = 0;
	int head = 0;
	int t = 0;
	int interval = 60 * 60;
	double max_pow = 0.;
	double actual = 0;
	double p_total = 0, p_extra = 0;
	double p_small = 0;
	int start = 700000, end = start + 2 * 60 * 60;
	int Ind = 0;
	int Maintain = 0, Up = 1, Down = 2;
	double DG= 0;
	double Mod = Capacity / 60 * 0.1;
	for (int i = 0; i < total_time; i ++){
		double small = 0;
		if (Ind == Up) DG += Mod; else if (Ind == Down) DG -= Mod;
		for (; head < sum && data[head].start == i; head ++){
			if (data[head].end - data[head].start < 60 * 10){
				small += data[head].power;
			}
			s.insert(make_pair(data[head].end, data[head].power));	
			cur += data[head].power;
			t  ++;
		}
		if (cur > DG) Ind = Up; else if (DG - Mod > cur) Ind = Down; else Ind = Maintain;
		if (i >= start && i <= end) printf("%f\n", DG);
		p_total += cur;
		actual += cur; 
		p_small = max(p_small, small);
		max_pow = (cur - max_pow > eps)? cur: max_pow; 
		while (!s.empty() && (*s.begin()).end == i + 1){
			cur -= (*s.begin()).power;
			//		printf("%lf\n", cur);
			s.erase(s.begin());	
			t --;
		}	
	} 
}

